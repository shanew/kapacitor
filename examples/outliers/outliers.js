'use strict';

// Load modules

const Process = require('process');
const Kapacitor = require('../../lib/index');

// Optional logger
const Winston = require('winston');
const logger = new (Winston.Logger) ({
   transports: [
       new (Winston.transports.File) ({ filename: '/tmp/mirror.log' })
   ]
});

// Shortcuts

const Agent = Kapacitor.Agent;
const Handler = Kapacitor.Handler;
const Udf = Kapacitor.Udf;
const Helpers = Kapacitor.UdfHelpers;

// Declare internals

const internals = {};

internals.State = function (size) {

    this._entries = [];
}

internals.State.prototype.reset = function () {

    this._entries = [];
};

internals.State.prototype.update = function (value, point) {

    this._entries.push([value, point]);
};

internals.State.prototype.outliers = function(scale) {


};

internals.AvgHandler = class AvgHandler extends Handler {

    constructor() {
        super();

        this._field = null;
        this._size = 0;
        this._as = 'avg';
        this._state = {};
    }

    info() {

        const response = new Udf.InfoResponse();
        response.setWants(Udf.EdgeType.STREAM);
        response.setProvides(Udf.EdgeType.STREAM);

        const options = [];

        let optionsEntry = new Udf.OptionsEntry();
        optionsEntry.setKey('field');
        optionsEntry.setValue(Udf.OptionValue.ValueCase.STRINGVALUE);
        options.push(optionsEntry);

        optionsEntry = new Udf.OptionsEntry();
        optionsEntry.setKey('size');
        optionsEntry.setValue(Udf.OptionValue.ValueCase.INTVALUE);
        options.push(optionsEntry);

        optionsEntry = new Udf.OptionsEntry();
        optionsEntry.setKey('as');
        optionsEntry.setValue(Udf.OptionValue.ValueCase.STRINGVALUE);
        options.push(optionsEntry);

        response.setOptionsList(options);

        return response;
    }

    init(initRequest) {
        let success = true;
        msg = '';

        const options = initRequest.getOptionsList();
        for (let option of options) {

            let name = option.getName();
            let values = option.getValuesList();

            if (name === 'field') {
                this._field = values[0].getStringvalue();
            }

            if (name === 'size') {
                this._size = values[0].getIntvalue();
            }

            if (name === 'as') {
                this._as = values[0].getStringvalue();
            }
        }


        if (!this._field) {
            success = false;
            msg += ' must supply field name';
        }

        if (this._size === 0) {
            success = false;
            msg += ' must supply window size';
        }

        if (this._size === 0) {
            success = false;
            msg += 'invalid as name';
        }

        const response = new Udf.InitResponse();
        response.setSuccess(success);
        response.setError(msg.substr(1));

        return response;
    }

    snapshot() {

        const response = new Udf.SnapshotResponse();
        response.setSnapshot(JSON.stringify(this._state));
        return response;
    }

    restore(restoreRequest) {
        let success = false;
        let msg = '';

        try {
            const snapshot = JSON.parse(restoreRequest.getSnapshot());
            for (let group of Object.keys(snapshot)) {
                this._state[group] = new State(0)
                this._state[group].restore(snapshot);
            }

            success = true;
        }
        catch (ex) {
            success = false;
            msg = ex.message;
        }

        const response = new Udf.RestoreResponse();
        response.setSuccess(success);
        response.setError(msg);
        return response;
    }

    beginBatch(begin) {
        throw new Error('not supported');
    }

    point(point) {

        const pointCopy = point.cloneMessage();
        pointCopy.clearFieldsintList();
        pointCopy.clearFieldsdoubleList();
        pointCopy.clearFieldsstringList();

        if (!this._state[pointCopy.group]) {
            this._state[pointCopy.group] = new State(this._size);
        }

        const field = Helpers.getDoubleField(this._field, pointCopy);
        const avg = this._state[pointCopy.group].update(field.getValue());

        const doubleEntry = new Udf.Point.FieldsDoubleEntry();
        doubleEntry.setKey(this._as);
        doubleEntry.setValue(avg);

        pointCopy.setFieldsdoubleList([doubleEntry]);

        return pointCopy;
    }

    endBatch(end) {
        throw new Error('not supported');
    }
};

if (require.main === module) {

    const handler = new internals.AvgHandler();
    const agent = new Agent(handler, null, null, logger);

    agent.start(() => {

        logger.log('info', 'Agent started.');
    });
}