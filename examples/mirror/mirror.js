'use strict';

// Load modules

const Process = require('process');
const Kapacitor = require('../../lib/index');

// Optional logger
const Winston = require('winston');
const logger = new (Winston.Logger) ({
   transports: [
       new (Winston.transports.File) ({ filename: '/tmp/mirror.log' })
   ]
});

// Shortcuts

const Agent = Kapacitor.Agent;
const Server = Kapacitor.Server;
const Handler = Kapacitor.Handler;
const Udf = Kapacitor.Udf;

// Declare internals

const internals = {};

internals.MirrorHandler = class MirrorHandler extends Handler {

    info() {
        const response = new Udf.Response();

        const info = new Udf.InfoResponse();
        info.setWants(Udf.EdgeType.STREAM);
        info.setProvides(Udf.EdgeType.STREAM);

        response.setInfo(info);
        return response;
    }

    init(initRequest) {

        const response = new Udf.InitResponse();
        response.setSuccess(true);
        return response;
    }

    snapshot() {

        const response = new Udf.SnapshotResponse();
        response.setSnapshot('');
        return response;
    }

    restore(restoreRequest) {

        const response = new Udf.RestoreResponse();
        response.setSuccess(false);
        response.setError('not implemented');
        return response;
    }

    beginBatch(begin) {
        throw new Error('not supported');
    }

    point(point) {

        const pointCopy = point.cloneMessage();
        return pointCopy;
    }

    endBatch(end) {
        throw new Error('not supported');
    }
};

if (require.main === module) {

    let path = "/tmp/mirror.sock"

    if (process.argv.length === 3) {
        path = process.argv[2];
    }

    const handler = new internals.MirrorHandler();
    const server = new Server(path, handler, logger);
    server.serve(() => {

        // Server started.
        logger.log('info', 'Server started.');
    });
}