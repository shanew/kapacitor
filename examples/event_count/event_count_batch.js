'use strict';

// Load modules

const Process = require('process');
const Kapacitor = require('../../lib/index');
const InternalHelpers = require('../../lib/helpers');

// Optional logger
const Winston = require('winston');
const logger = new (Winston.Logger) ({
   transports: [
       new (Winston.transports.File) ({ filename: '/tmp/event_count.log' })
   ]
});

// Shortcuts
const Server = Kapacitor.Server;
const Agent = Kapacitor.Agent;
const Handler = Kapacitor.Handler;
const Udf = Kapacitor.Udf;
const Helpers = Kapacitor.UdfHelpers;

// Declare internals

const internals = {};

internals.State = function (threshold, asKey) {

    this._threshold = threshold;
    this._as = asKey;
    this._counter = {};
    this._tracking = {};
}

internals.State.prototype.reset = function () {

    this._counter = {};
    this._tracking = {};
};

internals.State.prototype.snapshot = function () {

    const snapshot = {
        threshold: this._threshold,
        as: this._as,
        counter: this._counter,
        tracking: this._tracking
    };

    return InternalHelpers.stringToUint8Array(JSON.stringify(snapshot));
}

internals.State.prototype.restore = function (data) {

    const snapshot = JSON.parse(InternalHelpers.uint8ArrayToString(data));

    this._threshold = snapshot.threshold;
    this._as = snapshot.as;
    this._counter = snapshot.counter;
    this._tracking = snapshot.tracking;
}

internals.State.prototype.update = function (point) {

    // Clone the point
    const pointCopy = point.cloneMessage();

    // Get the group and increment it based off
    // the number of times it has been seen.
    const group = pointCopy.getGroup();
    if (!this._counter[group]) {
        this._counter[group] = 1;
    } else {
        this._counter[group]++;
    }

    const entry = new Udf.Point.FieldsIntEntry();
    entry.setKey(this._as);
    entry.setValue(this._counter[group]);
    pointCopy.setFieldsintList([entry]);

    // Now keep track of this specific point
    if(!this._tracking[group]) {
        this._tracking[group] = [];
    }

    this._tracking[group].push(pointCopy);

};

internals.State.prototype.eventCounts = function () {

    let write = [];

    for (let group of Object.keys(this._tracking)) {

        if (this._tracking[group].length >= this._threshold) {
            write = write.concat(this._tracking[group]);
        }
    }

    this.reset();

    return write;
};


internals.EventCountHandler = class EventCountHandler extends Handler {

    constructor() {
        super();

        this._field = null;
        this._threshold = 0;
        this._as = 'event_count';

        // state tracks [group] -> counter
        this._state = {};
    }

    info() {
        const response = new Udf.Response();
        const info = new Udf.InfoResponse();
        info.setWants(Udf.EdgeType.BATCH);
        info.setProvides(Udf.EdgeType.BATCH);

        const options = [];

        let optionsEntry = new Udf.InfoResponse.OptionsEntry();
        optionsEntry.setKey('field');

        let optionInfo = new Udf.OptionInfo();
        optionInfo.setValuetypesList([proto.udf.ValueType.STRING]);

        optionsEntry.setValue(optionInfo);
        options.push(optionsEntry);

        optionsEntry = new Udf.InfoResponse.OptionsEntry();
        optionsEntry.setKey('threshold');

        optionInfo = new Udf.OptionInfo();
        optionInfo.setValuetypesList([proto.udf.ValueType.INT]);

        optionsEntry.setValue(optionInfo);
        options.push(optionsEntry);

        optionsEntry = new Udf.InfoResponse.OptionsEntry();
        optionsEntry.setKey('as');

        optionInfo = new Udf.OptionInfo();
        optionInfo.setValuetypesList([proto.udf.ValueType.STRING]);

        optionsEntry.setValue(optionInfo);
        options.push(optionsEntry);

        info.setOptionsList(options);

        response.setInfo(info);
        return response;
    }

    init(initRequest) {
        let success = true;
        let msg = '';

        const options = initRequest.getOptionsList();
        for (let option of options) {

            let name = option.getName();
            let values = option.getValuesList();

            if (name === 'field') {
                this._field = values[0].getStringvalue();
            }

            if (name === 'threshold') {
                this._threshold = values[0].getIntvalue();
            }

            if (name === 'as') {
                this._as = values[0].getStringvalue();
            }
        }


        if (!this._field) {
            success = false;
            msg += ' must supply field name';
        }

        if (this._threshold === 0) {
            success = false;
            msg += ' must be greater than 0';
        }

        if (this._as === 0) {
            success = false;
            msg += 'invalid as name';
        }


        this._state = new internals.State(this._threshold, this._as);

        const response = new Udf.Response();
        const init = new Udf.InitResponse();
        init.setSuccess(success);
        init.setError(msg.substr(1));
        response.setInit(init);

        return response;
    }

    snapshot() {
        const response = new Udf.Response();
        const snapshot = new Udf.SnapshotResponse();
        snapshot.setSnapshot(this._state.snapshot());
        response.setSnapshot(snapshot);
        return response;
    }

    restore(restoreRequest) {
        let success = false;
        let msg = '';

        try {
            this._state.restore(restoreRequest.getSnapshot());
            success = true;
        }
        catch (ex) {
            success = false;
            msg = ex.message;
        }

        const response = new Udf.Response();
        const restore = new Udf.RestoreResponse();

        restore.setSuccess(success);
        restore.setError(msg);
        response.setRestore(restore);

        return response;
    }

    beginBatch(begin) {
        this._state.reset();

        const response = new Udf.Response();
        response.setBegin(begin.cloneMessage());
        return response;
    }

    point(point) {

        this._state.update(point);
        return null;
    }

    endBatch(end) {
        const responses = [];

        const eventCounts = this._state.eventCounts();
        for (let eventCount of eventCounts) {

            const pointResponse = new Udf.Response();
            pointResponse.setPoint(eventCount);
            responses.push(pointResponse);
        }

        const endResponse = new Udf.Response();
        endResponse.setEnd(end.cloneMessage());
        responses.push(endResponse);

        return responses;
    }
};

if (require.main === module) {

    const handler = new internals.EventCountHandler();
    const agent = new Agent(handler, null, null, logger);

    agent.start(() => {

        logger.log('info', 'Agent started.');
    });

    // let path = "/tmp/mirror.sock"

    // if (process.argv.length === 3) {
    //     path = process.argv[2];
    // }

    // const handler = new internals.EventCountHandler();
    // const server = new Server(path, handler, logger);
    // server.serve(() => {

    //     // Server started.
    //     logger.log('info', 'Server started.');
    // });
}