'use strict';

// Declare internals
const internals = {};

// Internal constants
const mask32uint = Math.pow(2, 32) - 1;
const byteSize = 8;
const shiftSize = byteSize - 1;
const varintMoreMask = Math.pow(2, shiftSize);
const varintMask = varintMoreMask - 1;

internals.decodeUvarint32 = function (reader) {

    let result = 0;
    let shift = 0;
    let b = null;

    while (true) {
        b = reader.read(1)
        if (!b) {
            throw new internals.EOF;
        }

        result |= ((b[0] & varintMask) << shift)
        if (!(b[0] & varintMoreMask)) {
            result &= mask32uint;
            return result;
        }

        shift += shiftSize;
        if (shift > 32) {
            throw new Error("too many bytes when decoding varint, larger than 32bit uint")
        }
    }
};

internals.encodeUvarint = function (value) {

    let byteArray = [];

    let bits = value & varintMask;
    value >>= shiftSize;
    while (value) {
        byteArray.push((varintMoreMask | bits));
        bits = value & varintMask;
        value >>= shiftSize;
    }

    byteArray.push(bits);

    return new Uint8Array(byteArray);
};

internals.uint8ArrayToBuffer = function (array) {

    const buffer = new Buffer(array.byteLength);

    for (let i = 0; i < buffer.length; ++i) {
        buffer[i] = array[i];
    }

    return buffer;
};

internals.stringToUint8Array = function(str) {

    let array = new Uint8Array(str.length);
    for (let i = 0; i < str.length; ++i) {
        array[i] = str.charCodeAt(i);
    }
    return array;
},

internals.uint8ArrayToString = function(array) {

    let str = '';
    for (let i = 0; i < array.length; ++i) {
        str += String.fromCharCode(array[i]);
    }
    return str;
},

internals.EOF = function() {};
internals.EOF.prototype = new Error();

exports = module.exports = {
    EOF: internals.EOF,
    stringToUint8Array: internals.stringToUint8Array,
    uint8ArrayToString: internals.uint8ArrayToString,
    uint8ArrayToBuffer: internals.uint8ArrayToBuffer,
    encodeUvarint: internals.encodeUvarint,
    decodeUvarint32: internals.decodeUvarint32
};