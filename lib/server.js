'use strict';

// Load modules
const Process = require('process');
const Net = require('net');
const Fs = require('fs');
const Agent = require('./agent');

// Declare internals
const internals = {};

exports = module.exports = internals.Server = function (socketPath, handler, logger) {

    this._socketPath = socketPath;
    this._handler = handler;
    this._logger = logger || console;
};

internals.Server.prototype.serve = function(callback) {

    this._server = Net.createServer((socket) => {

        // New socket connection, create the agent.
        const agent = new Agent(this._handler, socket, socket, this._logger);

        agent.start(() => {

            // Finished
            this._logger.log('info', 'Agent started.');
        })
    });

    // Handle errors here
    this._server.on('error', (err) => {

        this._logger.log('error', `Error occurred: ${err}.`);
         throw err;
    });

    // Handle the case where the application is killed.
    Process.on('SIGINT', () => {

        // Stop the server
        this.stop(() => {
            this._logger.log('SIGINT');
            Process.exit();
        })
    })

    //do something when app is closing
    Process.on('exit', () => {

        // Stop the server
        this.stop(() => {
            this._logger.log('exit');
        })
    });

    //catches uncaught exceptions
    Process.on('uncaughtException', () => {

        // Stop the server
        this.stop(() => {
            this._logger.log('uncaughtException');
            Process.exit();
        })
    });

    // Listen on the specified socket path.
    this._server.listen(this._socketPath, callback);
};

internals.Server.prototype.stop = function (done) {

    if (this._server) {
        return this._server.close(() => {
            this._server = null;
            this._cleanUp();

            return done();
        });
    }

    this._cleanUp();
    return done();
};

internals.Server.prototype._cleanUp = function () {

    try {
        Fs.unlinkSync(this._socketPath);
    }
    catch(ex) {
        // do nothing
    }
};