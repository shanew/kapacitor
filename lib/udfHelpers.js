'use strict';

// Declare internals
const internals = {};

internals.getField = function (key, listType, point) {

    const fields = point[listType]();
    for (let field of fields) {
        if (field.getKey() === key) {
            return field;
        }
    }

    return null;
};

internals.getDoubleField = function (key, point) {

    return internals.getField(key, 'getFieldsdoubleList', point);
};

internals.getIntField = function (key, point) {

    return internals.getField(key, 'getFieldsintList', point);
};

internals.getStringField = function (key, point) {

    return internals.getField(key, 'getFieldsstringList', point);
};

exports = module.exports = {
    getDoubleField: internals.getDoubleField,
    getIntField: internals.getIntField,
    getStringField: internals.getStringField
};