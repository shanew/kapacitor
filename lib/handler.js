'use strict';

exports = module.exports = class Handler {
    info() { }
    init(initRequest) { }
    snapshot() { }
    restore(restoreRequest) { }
    beginBatch(begin) { }
    point(point) { }
    endBatch(end) { }
};