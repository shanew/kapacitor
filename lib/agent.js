'use strict';

// Load modules
const Process = require('process');
const Udf = require ('./udf_pb');
const Helpers = require('./helpers');
const Co = require('co');

// Shortcuts
const EOF = Helpers.EOF;

// Declate internals
const internals = {};

exports = module.exports = internals.Agent = function (handler, istream, ostream, logger) {

    this._handler = handler;
    this._istream = istream || Process.stdin;
    this._ostream = ostream || Process.stdout;
    this._logger = logger || console;

    // Used in the read loop
    this._size = 0;
};

internals.Agent.prototype._readLoop = function *() {

    try {

        let chunk = null;
        do {

            if (this._size === 0) {
                this._size = Helpers.decodeUvarint32(this._istream);
            }

            chunk = this._istream.read(this._size);

            if (chunk) {
                let request = Udf.Request.deserializeBinary(new Uint8Array(chunk));

                // yield the request object
                yield request;

                // reset the size
                this._size = 0;
            }

        } while (chunk);
    }
    catch (e) {
        if (!(e instanceof EOF)) {
            console.log(e);
            // log a specific type of file.
        }
    }
}

internals.Agent.prototype.start = function (callback) {

    this._istream.on('readable', () => {

        return Co(function *() {

            // Get the iterator which yields requests as they are
            // received.
            const requests = this._readLoop();

            // Iterate until there are no more requests to process.
            let request = null;
            while ((request = requests.next()) && !request.done) {

                // Process specific request.
                const result = this._processRequest(request.value);
                if (!result) {
                    continue;
                }

                // Returns an array of functions which write a specific 'Responses' to
                // the output stream.  Called iteratively to ensure data is flushed in
                // the proper order.
                for (let item of result) {
                    yield item();
                }
            }
        }.bind(this));

    });

    this._istream.on('end', () => {
        callback();
    });

    this._ostream.on('close', () => {
    });
};

internals.Agent.prototype._processRequest = function (request, callback) {

    let responses = this._getResponses(request);

    if (!responses) {
        return null;
    }

    // Instead of giving the handler a reference to the agent, in order to write responses,
    // the NodeJS implementation expects an array of 'Responses' that it will write in the order
    // they are received.
    if (Array.isArray(responses)) {
        return responses.map((response) => { return this._writeResponse(response) });
    }

    return [this._writeResponse(responses)];
}

internals.Agent.prototype._getResponses = function (request) {

    const code = request.getMessageCase();

    if (code === Udf.Request.MessageCase.KEEPALIVE) {

        const time = request.getKeepalive().getTime();
        const keepAlive = new Udf.KeepaliveResponse();
        keepAlive.setTime(time);

        let result = new Udf.Response();
        result.setKeepalive(keepAlive);
        return result;
    }
    else if (code === Udf.Request.MessageCase.INFO) {
        return this._handler.info();
    }
    else if (code === Udf.Request.MessageCase.INIT) {
        return this._handler.init(request.getInit());
    }
    else if (code === Udf.Request.MessageCase.SNAPSHOT) {
        return this._handler.snapshot();
    }
    else if (code === Udf.Request.MessageCase.RESTORE) {
        return this._handler.restore(request.getRestore());
    }
    else if (code === Udf.Request.MessageCase.BEGIN) {
        return this._handler.beginBatch(request.getBegin());
    }
    else if (code === Udf.Request.MessageCase.POINT) {
        return this._handler.point(request.getPoint());
    }
    else if (code === Udf.Request.MessageCase.END) {
        return this._handler.endBatch(request.getEnd());
    }
    else {
        // either a Udf.Request.MessageCase.MESSAGE_NOT_SET request,
        // or we currently don't yet interpret the specific code.
        return null;
    }
}

internals.Agent.prototype._writeResponse = function (response) {

    // Deserialize the response
    const data = response.serializeBinary();

    // Encode the message length
    let size = Helpers.encodeUvarint(data.byteLength);

    const ostream = this._ostream;

    // Now write both
    return function() {
        return new Promise((resolve, reject) => {
            ostream.write(Helpers.uint8ArrayToBuffer(size), () => {
                ostream.write(Helpers.uint8ArrayToBuffer(data), resolve);
            });
        });
    };
};
